CREATE TABLE IF NOT EXISTS cars  (name varchar(100), price float, model varchar(50), year int);

INSERT INTO cars (name, price, model, year) values ('santa fe', 20000, 'hyundai', 2018);

INSERT INTO cars (name, price, model, year) values ('i30', 15000, 'hyundai', 2019),
                                                   ('i20', 12000, 'hyundai', 2018),
                                                   ('Tucson', 20000, 'hyundai', 2013),
                                                   ('Accent', 18000, 'hyundai', 2020),
                                                   ('Camry', 24000, 'Toyota', 2017),
                                                   ('RAV4', 22000, 'Toyota', 2015),
                                                   ('Land Cruiser', 30000, 'Toyota', 2014);

CREATE TABLE company (name varchar(50) PRIMARY KEY unique, country varchar(100), year_est int);

INSERT INTO company (name, country, year_est) values ('hyundai', 'South Korea', 1967),  ('Toyota', 'Japan', 1937);

select model, max(price)
from cars right join company c on cars.model =  c.name
WHERE year > 2015
GROUP by model
HAVING max(price) >= 20000;


/**** HOME WORK *****/
ALTER TABLE cars ADD COLUMN IF NOT EXISTS price_uah float DEFAULT 0;

CREATE OR REPLACE FUNCTION convert_uah_price()
    RETURNS TRIGGER
    LANGUAGE plpgsql
AS
$$
BEGIN
    NEW.price_uah:= NEW.price * 27;
    RETURN NEW;
END
$$;

CREATE TRIGGER convert_price
    BEFORE INSERT OR UPDATE ON cars
    FOR EACH ROW
EXECUTE PROCEDURE convert_uah_price();

INSERT INTO cars (name, price, model, year) values ('HB20', 15320, 'hyundai', 2019);

SELECT name, price, price_uah FROM cars;
/**** HOME WORK *****/
